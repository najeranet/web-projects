// import the react and ReactDOM libraries
import React from   'react'
import ReactDOM from 'react-dom'




// Create a react component
const App = () => {
	const buttonText = {text: 'Click Me!'}
	const labelText = 'Enter name:'
	return (
			<div>
					<label class="label" for="name">
					{labelText}
					</label>
					<input id="name" type="Name" />
					<button style={{backgroundColor: 'blue', color: 'white'}}>
						{buttonText.text}
					</button>
			</div>
	)
};


// take the react component and show it opn the screen
ReactDOM.render(<App />, document.querySelector('#root'));


