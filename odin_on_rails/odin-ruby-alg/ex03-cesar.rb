def cesar_cipher(offset, string)
	encrypt_str = ""
	alph = "abcdefghijklmnopqrstuvwxyz"

	string.split("").each{ |c| 
		if c != " "
			c_index = alph.index(c)
			new_pos = c_index .+ offset
			new_alph_index = new_pos % 26
			encrypt_str = encrypt_str.concat(alph[new_alph_index])
		else
			encrypt_str = encrypt_str.concat(" ")
		end
	}
	return encrypt_str
end

puts(cesar_cipher(3, "abc"))
puts ''
puts(cesar_cipher(7, "hola soy una cadena de texto para encriptar"))


puts ''
puts 'Another cesar cipher'
puts ''

puts "Welcome to cipher"
puts "Please input the phrase you wish to encode"
phrase = gets.chomp

puts "And now a number for the key"
key = gets.chomp.to_i

def cipher(phrase, key)
	output = ""
	phraseArr = phrase.split("")
	phraseArr.each do |chr| 
		new_position =chr.ord + key
		diff = 0
		if (chr.ord) > 64 && (chr.ord) < 91 && new_position > 90
			diff = new_position - 90
			new_position = 65 + diff
		elsif (chr.ord) > 97 && (chr.ord) < 123 && new_position	> 122
			diff = new_position - 122
			new_position = 96 + diff
		elsif (chr.ord) > 31 && (chr.ord) < 48
			new_position = chr.ord
		end

		output += new_position.chr
	end
	return output
end

puts ''
puts cipher(phrase, key)
puts ''
