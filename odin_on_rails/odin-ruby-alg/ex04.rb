# Loooooop !!!
=begin
# This part of code lookin to a web page and get tags

require 'open-uri'
def just_fetch_page(url)
    return open(url).read
end

def just_count_tags(page, tag)
    pattern = /<#{tag}\b/
    tags = page.scan(pattern)
    return tags.length
end

sites = ["http://www.wsj.com", "http://www.nytimes.com", "http://www.ft.com"]
tags = ["div","h1","h2","h3","img","p"]

sites.each do |url|
    puts "#{url} has: "
    tags.each do |tag|
        page = just_fetch_page(url)
        tag_count = just_count_tags(page, tag)
        puts "\t - #{tag_count} <#{tag}> tags"
    end
end
=end

# Mooore Looops Easy !!!

for current_iteration_number in 1..5 do 
    puts "Hello word, this is number #{current_iteration_number} "
end
#
puts ''
#
for i in 1..100
    puts i if i % 7 == 0 # imprime i, si, el modulo de 7 es igual a "0"
end
#
puts '"Numeros divesibles de 1 a 1000"'
#
for y in 1..25 do
    number_count = 0
    for x in 1..1000 do
        number_count += 1 if x % y ==0
    end
    puts "There are #{number_count} numbers divisible by #{y}, from 1 to 1000 "
end
#
puts 'other loops'
#
10.times do |k|
    puts "Number #{k+1} "
   #   times will start at 0, so on the 10th iteration, k is equal to 9
end
#
puts 'while loop'
#
x = 100
while x > 0
    x -= 1
    puts "This loop will run #{x} more times"
end
#
puts 'Doble for loop'
#
for row_num_1 in 1..9
    line = ""
    for col_num in 1..9
        line += "#{row_num_1 * col_num}\t"
    end
    puts line
end
#
puts 'Doble for loop with .each'
#
(1..9).each do |row_num|
    line = ""
    (1..9).each {|col_num| line += "#{row_num * col_num}\t"}
    puts line
end