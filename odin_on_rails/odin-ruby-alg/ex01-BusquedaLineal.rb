=begin
Este es un algoritmo de búsqueda básico. 
Se itera en una colección o en una estructura de datos 
hasta encontrar un valor que coincida.
=end

def findIndex(values, target) 
    values.each_with_index do |values, i|
        return i if values == target
    end
end

puts("The target is: ", findIndex([4, 8, 15, 16, 23, 42], 15))
