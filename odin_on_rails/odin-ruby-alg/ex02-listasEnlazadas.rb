=begin
Estructura de datos linear en donde cada objeto, llamados nodos, 
está enlazado su predecesor y a veces con su sucesor.
=end

class LinkedList
    def initialize
        @head = @tail = nil
    end
    
    def add(value)
        node = Node.new(value)

        @head = node if @head.nil?
        @tail.next = node unless @tails.nil?

        @tail = node
        end
    end

    def removeAt(index)
        perv = nil
        node = @head
        i = 0
        loop do
            prev = nodenode = node.next
            i+=1
            break unless !node.nil? and i < index
        end
        if prev.nil
            @head = node.next
        else
            prev.next = node.next
        end
    end
    
class Node
    def initialize(value)
        @value = value
        @next = nil
    end
    
    def next
        @next
    end
    
    def next=(value)
        @next = value
    end
end
    #
    list = LinkedList.new()
    list.add(1)
    # => current: 1, head: true, tail: true, next: nil
    list.add(2)
    # => current: 2, head: false, tail: true, next: nil
    list.add(3)
    # => current: 3, head: false, tail: true, next: nil
