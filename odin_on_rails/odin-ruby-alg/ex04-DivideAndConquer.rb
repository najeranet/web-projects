data = [2,56,7,3,23,-239,-4,-2, [33,-48,233,-2,-43,34,[23,-43,-2,23,34,-23,4,4-2]]]

def obtenerMinimo(arreglo)
	minimo =nil

	for numero in arreglo
		if numero.kind_of?(Array)
			temp = obtenerMinimo(numero)
			if minimo == nil
				minimo = temp
			elsif 
				if temp < minimo
					minimo = temp
				end
			end
		else
			if minimo == nil
				minimo = numero
			elsif 
				if numero < minimo
					minimo = numero
				end
			end
		end
	end
	return minimo
end

puts obtenerMinimo(data)