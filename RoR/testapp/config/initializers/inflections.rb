# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )
# end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end

#chekout  https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
#	for more explain
Rails.application.config.content_security_policy do |p|
	# p.default_src :self, :https
	p.font_src :self, :https, :data
	p.img_src :self, :https, :data
	p.object_src :none
	# p.script_src :self, :https
	p.style_src :self, :https, :unsafe_inline
	# Specify URI for violation reports
	# p.report_uri "/csp-violation-report-endpoint"
end