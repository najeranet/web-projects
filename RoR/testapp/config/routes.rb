Rails.application.routes.draw do
  get 'page/Index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'page#Index'
end
